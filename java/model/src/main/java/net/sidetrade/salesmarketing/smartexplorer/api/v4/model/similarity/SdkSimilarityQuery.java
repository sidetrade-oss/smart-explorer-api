package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.similarity;

import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.search.SdkSearchFilter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

public class SdkSimilarityQuery {

    @JsonProperty(required = false)
    @JsonPropertyDescription("The lists filter")
    public SdkSearchFilter lists;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The countries filter")
    public SdkSearchFilter countries;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The target results page")
    public Integer page;
}
