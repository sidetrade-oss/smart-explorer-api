package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

public class SdkEntityType {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The entity type identifier")
    public String id;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The entity type name")
    public String name;
}
