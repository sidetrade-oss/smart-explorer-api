package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

public class SdkEstimation {
    @JsonProperty(required = false)
    @JsonPropertyDescription("The company revenue estimation")
    public Float revenueEstimation;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company headcount estimation")
    public Float headcountEstimation;
}