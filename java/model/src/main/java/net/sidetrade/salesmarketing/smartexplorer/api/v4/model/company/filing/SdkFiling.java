package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.filing;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.net.URL;
import java.util.Date;

public class SdkFiling {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The filing identifier")
    public String id;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The filing type (creation, account deposit, etc)")
    public String type;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The filing source URL")
    public URL url;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The filing publication date")
    public Date publicationDate;
}
