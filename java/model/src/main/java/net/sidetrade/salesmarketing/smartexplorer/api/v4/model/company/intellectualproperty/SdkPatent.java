package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.intellectualproperty;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.util.Date;
import java.util.Map;

public class SdkPatent {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The patent identifier")
    public String id;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The patent title")
    public String title;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The patent categories")
    public Map<String, String> categories;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The patent application date")
    public Date applicationDate;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The patent publication date")
    public Date publicationDate;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The patent type (company is owner, or applicant only)")
    public SdkPatentType type;

    //

    public enum SdkPatentType {
        @JsonPropertyDescription("Company is owner of the patent")
        OWNER,
        @JsonPropertyDescription("Company is only the patent applicant")
        APPLICANT_ONLY;
    }
}
