package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.util.Set;

public class SdkCategory {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The category name")
    public String name;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The category sectors")
    public Set<String> sectors;
}
