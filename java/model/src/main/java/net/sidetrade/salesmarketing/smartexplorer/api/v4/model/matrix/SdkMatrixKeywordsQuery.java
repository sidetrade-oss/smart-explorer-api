package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.matrix;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.util.Set;

public class SdkMatrixKeywordsQuery {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The target companies list")
    public Set<String> companyIds;

    @JsonPropertyDescription("The companies list used as context for keyword weights computing.")
    public Set<String> context;

    @JsonPropertyDescription("The needed keywords count")
    public Integer count;

    @JsonPropertyDescription("The words to be blacklisted.")
    public Set<String> blacklist;

    @JsonPropertyDescription("Only words from those langs will be returned.")
    public Set<String> langs;
}
