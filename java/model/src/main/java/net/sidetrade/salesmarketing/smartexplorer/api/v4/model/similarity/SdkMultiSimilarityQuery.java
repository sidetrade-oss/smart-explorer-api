package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.similarity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.util.Set;

public class SdkMultiSimilarityQuery extends SdkSimilarityQuery {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The target company ids")
    public Set<String> ids;
}
