package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.util.Map;
import java.util.Date;

public class SdkIdentity {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The company official name")
    public String officialName;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company commercial name")
    public String commercialName;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company entity type")
    public SdkEntityType entityType;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The company creation date")
    public Date creationDate;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company termination date (if applicable)")
    public Date terminationDate;

    @JsonProperty(required = false)
    @JsonPropertyDescription("All the available descriptions for the company, by source")
    public Map<DescriptionSource, String> descriptions;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The best description found for the company")
    public String description;

    //

    public static enum DescriptionSource {

        @JsonPropertyDescription("Description issued when company was created")
        COMPANY_STATEMENT,
        @JsonPropertyDescription("Description extracted from the company website")
        WEBSITE,
        @JsonPropertyDescription("Description extracted from the 'about' field on Facebook API")
        FACEBOOK_ABOUT,
        @JsonPropertyDescription("Description extracted from the 'description' field on Facebook API")
        FACEBOOK_DESCRIPTION,
        @JsonPropertyDescription("Description extracted from the 'about' field on Twitter API")
        TWITTER_ABOUT
    }
}
