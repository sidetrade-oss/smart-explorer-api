package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.lists;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

public class SdkListInfo {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The list identifier")
    public String id;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The list name")
    public String name;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The list description")
    public String description;

    @JsonProperty(required = false)
    @JsonPropertyDescription("True if the list is shared, false otherwise")
    public Boolean shared;
}
