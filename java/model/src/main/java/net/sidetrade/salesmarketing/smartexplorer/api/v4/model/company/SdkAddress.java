package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

public class SdkAddress {

    @JsonProperty(required = false)
    @JsonPropertyDescription("The address line")
    public String address;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The address zip code")
    public String zipcode;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The address city")
    public String city;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The address country")
    public String country;
}
