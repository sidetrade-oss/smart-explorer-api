package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.financial;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

public class SdkFinancialData {

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company revenue")
    public Long revenue;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company operating income")
    public Long operatingIncome;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The company employees count")
    public Integer employees;
}
