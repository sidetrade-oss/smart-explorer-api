package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.contact;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.net.URL;
import java.util.LinkedHashSet;

public class SdkContact {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The contact identifier")
    public String id;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The contact firstname")
    public String firstname;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The contact lastname")
    public String lastname;
    @JsonProperty(required = true)
    @JsonPropertyDescription("The contact full name")
    public String fullname;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The contact titles (sorted by priority)")
    public LinkedHashSet<String> titles;
    @JsonProperty(required = false)
    @JsonPropertyDescription("True if contact is a company head executive")
    public Boolean headExecutive;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The contact email")
    public String email;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The contact email generation score")
    public Double emailScore;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The contact email validation score")
    public Double emailValidityScore;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The contact Twitter profile URL")
    public URL twitter;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The contact LinkedIn profile URL")
    public URL linkedin;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The contact Viadeo profile URL")
    public URL viadeo;
}
