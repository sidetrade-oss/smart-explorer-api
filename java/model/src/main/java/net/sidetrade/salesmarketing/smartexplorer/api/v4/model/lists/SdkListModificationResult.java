package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.lists;

import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.util.Set;

public class SdkListModificationResult {

    @JsonPropertyDescription("Number of companies added")
    public Integer nbCompanyAdded;

    @JsonPropertyDescription("Number of companies removed")
    public Integer nbCompanyRemoved;

    @JsonPropertyDescription("Companies Not found - Companies ids in format 'CountryCode-LocalId' ex: UK-4303002020")
    public Set<String> companiesNotFound;
}
