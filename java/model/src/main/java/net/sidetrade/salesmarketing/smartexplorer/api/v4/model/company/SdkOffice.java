package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.util.Date;

public class SdkOffice {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The office NIC code (ex: SIRET for France)")
    public String nic;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The office name")
    public String name;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The office postal address")
    public SdkAddress address;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The office classification (ex: NAF for France)")
    public SdkClassification classification;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The office creation date")
    public Date creationDate;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The office termination date (if applicable)")
    public Date terminationDate;
}
