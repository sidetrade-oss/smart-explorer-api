package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.reconcile;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

public class SdkReconcileSuggestion extends SdkReconcileData {

    @JsonProperty(required = true)
    @JsonPropertyDescription("Score of this candidate")
    public double score;
    @JsonProperty(required = true)
    @JsonPropertyDescription("true if this candidate matches excactly the request")
    public boolean exactMatch;
    @JsonProperty(required = true)
    @JsonPropertyDescription("true if the establishment is a head office")
    public boolean headOffice;

}
