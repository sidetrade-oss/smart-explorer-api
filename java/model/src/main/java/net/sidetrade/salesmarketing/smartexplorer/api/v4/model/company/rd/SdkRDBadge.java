package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.rd;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

public class SdkRDBadge {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The badge identifier")
    public String code;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The badge label (french)")
    public String label;
}
