package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.fundraising;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.util.Currency;
import java.util.Set;

public class SdkFundraising {

    @JsonProperty(required = true)
    @JsonPropertyDescription("The fundraising identifier")
    public String id;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The fundraising amount")
    public Long amount;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The fundraising currency")
    public Currency currency;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The fundraising investors list")
    public Set<SdkFundraisingInvestor> investors;

    @JsonProperty(required = true)
    @JsonPropertyDescription("The media items associated to the fundraising")
    public Set<SdkFundraisingNews> news;
}
