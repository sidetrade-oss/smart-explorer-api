package net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.rd;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.time.Year;
import java.util.List;

public class SdkCollaborativeProject {

    @JsonProperty(required = false)
    @JsonPropertyDescription("The collaborative project type")
    public String type;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The collaborative project acronym")
    public String acronym;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The collaborative project full label")
    public String label;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The collaborative project summary")
    public String description;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The collaborative project beginning year")
    public Year year;
    @JsonProperty(required = false)
    @JsonPropertyDescription("The collaborative project duration (months)")
    public Integer duration;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The collaborative project total budget (euros)")
    public Long budget;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The collaborative project website URL")
    public String url;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The collaborative project members")
    public List<SdkCollaborativeProjectMember> members;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The collaborative project themes")
    public List<String> themes;

    @JsonProperty(required = false)
    @JsonPropertyDescription("The collaborative project label for the call")
    public String call;
}
