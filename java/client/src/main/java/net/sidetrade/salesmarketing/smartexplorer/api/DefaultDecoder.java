package net.sidetrade.salesmarketing.smartexplorer.api;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import feign.FeignException;
import feign.Response;
import feign.codec.Decoder;
import feign.codec.StringDecoder;
import feign.jackson.JacksonDecoder;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.Year;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.function.Function;

public class DefaultDecoder implements Decoder {

    protected static final SimpleModule MODULE = new SimpleModule();
    static {
        MODULE.addKeyDeserializer(Year.class, new YearKeyDeserializer());
        MODULE.addDeserializer(Year.class, new YearDeserializer());
    }

    protected static final JacksonDecoder JSON = new JacksonDecoder(Arrays.asList(MODULE));
    protected static final StringDecoder TEXT = new StringDecoder();

    @Override
    public Object decode(Response response, Type type) throws IOException, FeignException {
        // If content type is specified
        final ContentType contentType = this.getContentType(response.headers());
        if(contentType != null) {
            // According to the content type
            switch(contentType) {
                case JSON:
                    return JSON.decode(response, type);
                case TEXT:
                    return TEXT.decode(response, type);
                default:
                    throw new IllegalStateException("Unsupported content type [" + contentType + "]");
            }
        }
        // Assume resource is text
        return TEXT.decode(response, type);
    }

    /**
     * Extract content type from the given headers.
     *
     * @param headers The HTTP headers
     * @return The extracted content type
     */
    protected ContentType getContentType(Map<String, Collection<String>> headers) {
        // If a Content-Type header is specified
        final Collection<String> contentTypes = headers.get("Content-Type");
        if(contentTypes != null && contentTypes.size() > 0) {
            // Return target enum value
            final String contentType = contentTypes.iterator().next().replaceAll(";.+$", "");
            switch(contentType) {
                case "application/json":
                    return ContentType.JSON;
                case "text/plain":
                    return ContentType.TEXT;
                default:
                    throw new IllegalStateException("Unsupported content type [" + contentType + "]");
            }
        }
        //
        return null;
    }

    //

    protected enum ContentType {
        JSON,
        TEXT;
    }

    //

    protected static final Function<String, Year> YEAR_MAPPER = value -> Year.of(Integer.valueOf(value));

    protected static class YearDeserializer extends JsonDeserializer<Year> {

        @Override
        public Year deserialize(JsonParser p, DeserializationContext context) throws IOException {
            return YEAR_MAPPER.apply(p.getText());
        }
    }

    protected static class YearKeyDeserializer extends KeyDeserializer {

        @Override
        public Object deserializeKey(String key, DeserializationContext context) {
            return YEAR_MAPPER.apply(key);
        }
    }
}
