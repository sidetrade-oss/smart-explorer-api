package com.cradar.api.v4;

import net.sidetrade.salesmarketing.smartexplorer.api.DefaultDecoder;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.SdkPaging;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.SdkCompany;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.company.SdkCompanySet;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.linkedcompanies.SdkLinkedCompany;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.lists.SdkListInfo;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.lists.SdkListItem;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.lists.SdkListModificationResult;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.matrix.SdkMatrixKeywordsQuery;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.matrix.SdkMatrixSliceQuery;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.reconcile.SdkReconcileData;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.reconcile.SdkReconcileSuggestion;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.search.SdkSearchQuery;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.search.SdkSearchResult;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.similarity.SdkMultiSimilarityQuery;
import net.sidetrade.salesmarketing.smartexplorer.api.v4.model.similarity.SdkSimilarityQuery;
import feign.*;
import feign.auth.BasicAuthRequestInterceptor;
import feign.jackson.JacksonEncoder;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Deprecated // Use SmartExplorerClient instead
public interface CRadarClient {

    String DEFAULT_API_URL = "https://api.c-radar.com";

    /**
     * Get a C-Radar company by id.
     *
     * @param id The target company id
     * @return The C-Radar company
     */
    @RequestLine("GET /v4/companies/{id}")
    SdkCompany getCompany(@Param("id") String id);

    /**
     * Get the crawl relevant text for the given company id.
     *
     * @param id The target company id
     * @return The crawl relevant text
     */
    @RequestLine("GET /v4/companies/{id}/crawl")
    String getCompanyCrawl(@Param("id") String id);

    /**
     * @return The current user C-Radar lists
     */
    @RequestLine("GET /v4/lists")
    List<SdkListInfo> getLists();

    /**
     * Get items from a C-Radar list.
     *
     * @param id   The target list id
     * @param page The page number
     * @return The corresponding list items
     */
    @RequestLine("GET /v4/lists/{id}/items?page={page}")
    SdkPaging<SdkListItem> getListItems(@Param("id") String id, @Param("page") int page);

    /**
     * Add items to a C-Radar list.
     *
     * @param id         The target list id
     * @param companyIds The companies to add to the list
     * @return The result of the operation
     */
    @RequestLine("POST /v4/lists/{id}/items")
    @Headers("Content-Type: application/json")
    SdkListModificationResult addToListItems(@Param("id") String id, Set<String> companyIds);

    /**
     * Remove items from a C-Radar list.
     *
     * @param id         The target list id
     * @param companyIds The companies to remove to the list
     * @return The result of the operation
     */
    @RequestLine("DELETE /v4/lists/{id}/items")
    @Headers("Content-Type: application/json")
    SdkListModificationResult removeFromListItems(@Param("id") String id, Set<String> companyIds);

    /**
     * Search for companies into C-Radar.
     *
     * @param query The search query
     * @return The search response
     */
    @RequestLine("POST /v4/companies/search")
    @Headers("Content-Type: application/json")
    SdkPaging<SdkSearchResult> search(SdkSearchQuery query);

    /**
     * Search for companies similar to a given one into C-Radar, using filters.
     *
     * @param id    The target company id
     * @param query The similarity query
     * @return The similarity response
     */
    @RequestLine("POST /v4/companies/{id}/similar")
    @Headers("Content-Type: application/json")
    SdkPaging<SdkSearchResult> getSimilar(@Param("id") String id, SdkSimilarityQuery query);

    /**
     * Search for companies similar to a given one into C-Radar.
     *
     * @param id The target company id
     * @return The similarity response
     */
    @RequestLine("POST /v4/companies/{id}/similar")
    SdkPaging<SdkSearchResult> getSimilar(@Param("id") String id);

    /**
     * Search for companies similar to many other ones into C-Radar.
     *
     * @param query The multi similarity query
     * @return The similarity response
     */
    @RequestLine("POST /v4/companies/similar")
    @Headers("Content-Type: application/json")
    SdkPaging<SdkSearchResult> getSimilar(SdkMultiSimilarityQuery query);

    /**
     * Search for companies similar to many other ones into C-Radar.
     *
     * @param query The similarity query
     * @param ids   The target company ids
     * @return The similarity response
     */
    default SdkPaging<SdkSearchResult> getSimilar(SdkSimilarityQuery query, String... ids) {
        // Build multi similarity query
        final SdkMultiSimilarityQuery multi = new SdkMultiSimilarityQuery();
        multi.lists = query.lists;
        multi.countries = query.countries;
        multi.page = query.page;
        multi.ids = Stream.of(ids).collect(Collectors.toSet());
        //
        return this.getSimilar(multi);
    }

    /**
     * Search for companies similar to many other ones into C-Radar.
     *
     * @param ids The target company ids
     * @return The similarity response
     */
    default SdkPaging<SdkSearchResult> getSimilar(String... ids) {
        // Build multi similarity query
        final SdkMultiSimilarityQuery multi = new SdkMultiSimilarityQuery();
        multi.ids = Stream.of(ids).collect(Collectors.toSet());
        //
        return this.getSimilar(multi);
    }

    /**
     * Get keywords from the company matrix.
     *
     * @param query The keywords query
     * @return The corresponding keywords
     */
    @RequestLine("POST /v4/matrix/keywords")
    @Headers("Content-Type: application/json")
    Map<String, Double> getMatrixKeywords(SdkMatrixKeywordsQuery query);

    /**
     * Get keywords from the company matrix.
     *
     * @param companyIds The target company ids
     * @param count      The needed keywords count
     * @return The corresponding keywords
     */
    default Map<String, Double> getMatrixKeywords(Set<String> companyIds, Set<String> context, int count,
                                                  Set<String> blacklist, Set<String> langs) {
        //
        final SdkMatrixKeywordsQuery query = new SdkMatrixKeywordsQuery();
        query.companyIds = companyIds;
        query.context = context;
        query.count = count;
        query.blacklist = blacklist;
        query.langs = langs;
        //
        return this.getMatrixKeywords(query);
    }

    /**
     * Get a slice from the company matrix.
     *
     * @param query The slice query
     * @return The corresponding slice
     */
    @RequestLine("POST /v4/matrix/slice")
    @Headers("Content-Type: application/json")
    Map getMatrixSlice(SdkMatrixSliceQuery query);

    /**
     * Get a slice from the company matrix.
     *
     * @param companyIds The target company ids
     * @param format         The slice output format
     * @param usePython2     If true, slice should be generated using Python2 version
     * @return The corresponding slice
     */
    default Map getMatrixSlice(Set<String> companyIds, SdkMatrixSliceQuery.Format format, boolean usePython2) {
        //
        final SdkMatrixSliceQuery query = new SdkMatrixSliceQuery();
        query.companyIds = companyIds;
        query.format = format;
        query.usePython2 = usePython2;
        //
        return this.getMatrixSlice(query);
    }

    /**
     * Reconcile a company using the given info.
     *
     * @param query The reconcile data
     * @return The corresponding candidates
     */
    @RequestLine("POST /v4/reconcile")
    @Headers("Content-Type: application/json")
    List<SdkReconcileSuggestion> reconcile(SdkReconcileData query);

    /**
     * Reconcile a list of companies using the given info (limited to 1000).
     *
     * @param queries The list of reconcile data
     * @return The list of corresponding candidates
     */
    @RequestLine("POST /v4/reconcile/bulk")
    @Headers("Content-Type: application/json")
    List<List<SdkReconcileSuggestion>> reconcileBulk(List<SdkReconcileData> queries);

    /**
     * Get a C-Radar companies id (limited to 1000)
     * <p>
     * Raise a HTTP 400 Bad Request if more than 1000 companies are requested.
     *
     * @param companyIds The reconcile data
     * @return The C-Radar company
     */
    @RequestLine("POST /v4/companies/find")
    @Headers("Content-Type: application/json")
    SdkCompanySet getCompanies(Set<String> companyIds);

    /**
     * Get linked companies of another company.
     *
     * @param id The target company id
     * @return The C-Radar linked companies
     */
    @RequestLine("GET /v4/companies/{id}/linked-companies")
    List<SdkLinkedCompany> getLinkedCompanies(@Param("id") String id);

    //

    /**
     * Connect a new V4 client to the C-Radar API.
     *
     * @param username The C-Radar username
     * @param password The password related to the given username
     * @param client   The HTTP client to be used
     * @return A new C-Radar client
     */
    static CRadarClient connect(String username, String password, Client client, String url) {
        return Feign.builder()
                .client(client)
                .decoder(new DefaultDecoder())
                .encoder(new JacksonEncoder())
                .requestInterceptor(new BasicAuthRequestInterceptor(username, password))
                .target(CRadarClient.class, url);
    }
    /**
     * Connect a new V4 client to the C-Radar API.
     *
     * @param username The C-Radar username
     * @param password The password related to the given username
     * @param client The HTTP client to be used
     * @return A new C-Radar client
     */
    static CRadarClient connect(String username, String password, Client client) {
        return CRadarClient.connect(username, password, client,DEFAULT_API_URL);

    }

    /**
     * Connect a new V4 client to the C-Radar API.
     *
     * @param username The C-Radar username
     * @param password The password related to the given username
     * @return A new C-Radar client
     */
    static CRadarClient connect(String username, String password) {
        return CRadarClient.connect(username, password, new Client.Default(null, null),DEFAULT_API_URL);
    }

    /**
     * Connect a new V4 client to the C-Radar API.
     *
     * @param username The C-Radar username
     * @param password The password related to the given username
     * @return A new C-Radar client
     */
    static CRadarClient connect(String username, String password, String url) {
        return CRadarClient.connect(username, password, new Client.Default(null, null),url);
    }
}
